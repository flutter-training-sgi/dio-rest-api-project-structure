import 'dart:convert';

ProductModel productPriceFromJson(String str) => ProductModel.fromJson(json.decode(str));

String productPriceToJson(ProductModel data) => json.encode(data.toJson());

class ProductModel {
  ProductModel({
    required this.id,
    required this.quantity,
    required this.price,
  });

  int id;
  int quantity;
  double price;

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
    id: json["Id"],
    quantity: json["Quantity"],
    price: json["Price"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "Id": id,
    "Quantity": quantity,
    "Price": price,
  };
}